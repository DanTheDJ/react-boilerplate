
# __Boilerplate__

Add a description for __Boilerplate__...

## Installation

*Prerequisites*:  

 - Node  
 - NPM  

To install the dependencies, run `npm install`.  

## Usage

The following commands are available:  

`build:dev` - Builds the project using Webpack in development mode.  
`build:prod` - Builds the project using Webpack in production mode.  
`serve` - Builds the project using Webpack in development mode and starts a Webpack dev server on [http://locahost:8000](http://locahost:8000).  
`test` - Runs available test suites using Jest.  
`test:coverage` - Runs available test suites using Jest and produces test coverage reports.  

To run a command, type `npm run` followed by any of the commands above.  
