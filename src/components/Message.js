import React from 'react';

class Message extends React.Component {

    constructor(props)
    {

        super(props);

        this.state = {
            message: props.message
        };

        this.handleClick = this.handleClick.bind(this);

    }

    handleClick()
    {

        this.setState({
            message: 'Now it\'s Changed!'
        });

    }

    render()
    {

        return <React.Fragment>
            <h1>{this.state.message}</h1>
            <button onClick={this.handleClick}>Click Me!</button>
            </React.Fragment>;

    }

}

export default Message;