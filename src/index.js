import React from 'react';
import ReactDOM from 'react-dom';

import Message from './components/Message';

const App = function() {

    return <React.Fragment>
        <Message message="Hello World!"></Message>
        <Message message="Hello, Again"></Message>
    </React.Fragment>;

};

ReactDOM.render(<App></App>, document.getElementById('container'));